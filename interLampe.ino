#include <SPI.h> //bibliothèqe pour SPI
#include <Ethernet.h> //bibliothèque pour Ethernet
byte mac[] = {...}; //adresse mac de votre carte
byte ip[] = {...}; //adresse IP
EthernetServer serveur(80); // déclare l'objet serveur au port d'écoute 80

void setup() {
  pinMode(7, OUTPUT); //pin en mode OUTPUT
  Ethernet.begin (mac, ip); //initialisation de la communication Ethernet
  serveur.begin(); // démarre l'écoute
}
void loop() {
  EthernetClient client = serveur.available(); //on écoute le port
  if (client) { //si client existe
    if (client.connected()) { // si le client est connecté
      GET(client); //appel de la fonction de décodage
      client.println("HTTP/1.1 200 OK"); // type du HTML
      client.println("Content-Type: text/html; charset=ascii"); //type de fichier et encodage des caractères
      client.println("Connection: close");  // fermeture de la connexion quand toute la réponse sera envoyée
      client.println();
      client.println("<!DOCTYPE HTML>");
      client.println("<a href=?1 target=_self style=font-size:80px;>ON</a><br><br>");
      client.println("<a href=?0 target=_self style=font-size:80px;>OFF</a>");
      client.stop(); //on déconnecte le client
    }
  }
}
void GET(EthernetClient cl) {
  boolean lu = 0;
  while (cl.available()) {
    char c = cl.read();
    delay(1);
    if (c == '?' && lu == 0) {
      int i = cl.read() - '0';
      delay(1);
      digitalWrite(7, !i);
      delay(10);
      lu = 1;
    }
  }
}
